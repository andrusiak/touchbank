package com.touchbank.models;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Yurii Andrusiak[yuriy.andrusyak@gmail.com] on 28.06.2015.
 */
public class Transaction {
    public enum Category { YELLOW, CYAN, MAGENTA}

    Category type = Category.CYAN;
    Date time;
    String title;
    double amount;

    public Transaction(Category type, Date time, String title, double amount) {
        this.type = type;
        this.time = time;
        this.title = title;
        this.amount = amount;
    }

    public Category getType() {
        return type;
    }

    public void setType(Category type) {
        this.type = type;
    }

    public Date getTime() {
        return time;
    }

    public String getFormattedTime() {
        return new SimpleDateFormat("HH:mm").format(time);
    }


    public void setTime(Date time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
