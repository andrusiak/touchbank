package com.touchbank.models;

/**
 * Created by Yurii Andrusiak[yuriy.andrusyak@gmail.com] on 27.06.2015.
 */
public class Account {

    private double balance;
    private double depositEarnings;
    private double depositInterest;
    private double cashback;
    private double cashbackInterest;

    public Account(double balance, double depositEarnings, double depositInterest, double cashback, double cashbackInterest) {
        this.balance = balance;
        this.depositEarnings = depositEarnings;
        this.depositInterest = depositInterest;
        this.cashback = cashback;
        this.cashbackInterest = cashbackInterest;
    }

    public static Account getMockAccount(){
        return new Account(348938.01, 32022.92, 0.09, 173.92, 0.015);
    }
}