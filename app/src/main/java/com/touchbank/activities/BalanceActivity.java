package com.touchbank.activities;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.touchbank.R;
import com.touchbank.fragments.CurrentBalanceFragment;
import com.touchbank.fragments.RefillFragment;

import java.util.Locale;


public class BalanceActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance);

        // Set up the toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Show a back button and color it white
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

//      Currently TabLayout doesn't work correctly with custom views in tabs - extra padding appears
//      setupTabLayout();

        // So let's stick to RadioGroup  based tabs solution
        //TODO Remove this SHAME later
        setupRadioGroupTabs();
    }

    private void setupTabLayout() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);

        // Make tabs look more like on iOS
        View viewL = LayoutInflater.from(this).inflate(R.layout.tab_item, null);
        View viewR = LayoutInflater.from(this).inflate(R.layout.tab_item, null);

        viewL.setBackgroundResource(R.drawable.sel_tab_left);
        ((TextView)viewL.findViewById(R.id.tabText)).setText(R.string.balance_tab_current);
        viewR.setBackgroundResource(R.drawable.sel_tab_right);
        ((TextView)viewR.findViewById(R.id.tabText)).setText(R.string.balance_tab_refill);

        viewL.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        viewR.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        TabLayout.Tab leftTab = tabLayout.newTab().setCustomView(viewL);
        TabLayout.Tab rightTab = tabLayout.newTab().setCustomView(viewR);

        tabLayout.addTab(leftTab);
        tabLayout.addTab(rightTab);

//      Calling of tabLayout.setupWithViewPager(mViewPager) will replace the tabs.
//      Bind TabLayout and ViewPager manually here
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(this);
    }

    /**
     *  Tabs-like RadioGroup that is bound to ViewPager
     */
    private void setupRadioGroupTabs() {
        final RadioGroup tabs = (RadioGroup) findViewById(R.id.tabs);
        final AppCompatRadioButton leftTab = (AppCompatRadioButton) tabs.findViewById(R.id.leftTab);
        final AppCompatRadioButton rightTab = (AppCompatRadioButton) tabs.findViewById(R.id.rightTab);

        tabs.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                int position =0;
                for(int i=0; i<radioGroup.getChildCount();i++){
                    AppCompatRadioButton child  = (AppCompatRadioButton) radioGroup.getChildAt(i);
                    if(child.isChecked()){
                        position=i;
                    }
                }
                mViewPager.setCurrentItem(position);
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        leftTab.setChecked(true);
                        rightTab.setChecked(false);
                        break;
                    case 1:
                        leftTab.setChecked(false);
                        rightTab.setChecked(true);
                }
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            @Override
            public void onPageScrollStateChanged(int state) { }
        });
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return position==0 ? CurrentBalanceFragment.newInstance() : RefillFragment.newInstance();
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.balance_tab_current).toUpperCase(l);
                case 1:
                    return getString(R.string.balance_tab_refill).toUpperCase(l);
            }
            return null;
        }
    }
}
