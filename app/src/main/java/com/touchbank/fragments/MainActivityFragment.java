package com.touchbank.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.touchbank.R;
import com.touchbank.activities.BalanceActivity;
import com.touchbank.activities.TransactionActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivityFragment extends Fragment {

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.btnTransfers)
    public void openTransfers() {
        // TODO open Transfer activity

    }
    @OnClick(R.id.btnTransactions)
    public void openTransactions() {
        Intent intent = new Intent(this.getActivity(), TransactionActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @OnClick(R.id.btnBalance)
    public void openBalance() {
        Intent intent = new Intent(this.getActivity(), BalanceActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
