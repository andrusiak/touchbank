package com.touchbank.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.touchbank.R;
import com.touchbank.adapters.TransactionRecyclerAdapter;
import com.touchbank.models.Transaction;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class TransactionActivityFragment extends Fragment {

    @Bind(R.id.listView) RecyclerView mRecyclerView;
    private TransactionRecyclerAdapter mAdapter;

    public TransactionActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_transaction, container, false);
        ButterKnife.bind(this, v);

        configureRecyclerView();

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void configureRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);

//        RecyclerView.ItemDecoration itemDecoration = new DividerJavaDecorator(getActivity(), DividerJavaDecorator.VERTICAL_LIST);
//        mRecyclerView.addItemDecoration(itemDecoration);
//        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public void onTouchEvent(RecyclerView recycler, MotionEvent event) {
                //Handle on touch events
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            }

            @Override
            public boolean onInterceptTouchEvent(RecyclerView recycler, MotionEvent event) {
                return false;
            }
        });

        mAdapter = new TransactionRecyclerAdapter(getDummyTransactions());
        mRecyclerView.setAdapter(mAdapter);

    }

    private ArrayList<Object> getDummyTransactions() {
        HashMap<String, List<Transaction>> map = new HashMap<>();
        ArrayList<Transaction> transactions  = new ArrayList<>();
        transactions.add(new Transaction(Transaction.Category.MAGENTA, new Date(), "Отправка перевода", -203.00));
        transactions.add(new Transaction(Transaction.Category.MAGENTA, new Date(), "Получение перевода по ссылке", 189.00));
        transactions.add(new Transaction(Transaction.Category.MAGENTA, new Date(), "Оплата в Мегамаркете", -203.34));
        transactions.add(new Transaction(Transaction.Category.MAGENTA, new Date(), "Оплата в Сильпо", -132.32));
        map.put("7 января", transactions);
        map.put("5 января", transactions);
        map.put("4 января", transactions);

//        for(String key: map.keySet()){
//            list.add(key);
//            list.addAll(map.get(key));
//        }

        ArrayList<Object> list = new ArrayList<>();
        list.add("7 января");
        list.addAll(transactions);
        list.add("5 января");
        list.addAll(transactions);
        list.add("4 января");
        list.addAll(transactions);

        return list;
    }
}
