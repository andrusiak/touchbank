package com.touchbank.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.touchbank.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RefillFragment extends Fragment {

    private static final String TAG = "RefillFragment";
    @Bind({ R.id.cardNumber1, R.id.cardNumber2, R.id.cardNumber3, R.id.cardNumber4, R.id.expMonth, R.id.expYear, R.id.cvv, R.id.amount})
    List<AppCompatEditText> inputViews;
    int[] maxLengths= new int[]{4, 4, 4, 4, 2, 2, 3, Integer.MAX_VALUE};

    @Bind(R.id.btnSend)
    Button btnSend;

    public static RefillFragment newInstance() {
        RefillFragment fragment = new RefillFragment();
        return fragment;
    }

    public RefillFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_refill, container, false);
        ButterKnife.bind(this, v);

        for(int i=0; i<inputViews.size()-1;i++){
            final AppCompatEditText edit = inputViews.get(i);
            final AppCompatEditText editNext = inputViews.get(i+1);
            final int maxLength = maxLengths[i];

            edit.addTextChangedListener(new TextWatcher() {
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(s.length()==maxLength) editNext.requestFocus();
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            });
        }
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
