package com.touchbank.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.touchbank.R;

public class CurrentBalanceFragment extends Fragment {

    public static CurrentBalanceFragment newInstance() {
        CurrentBalanceFragment fragment = new CurrentBalanceFragment();
        return fragment;
    }

    public CurrentBalanceFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_current_balance, container, false);
    }
}
