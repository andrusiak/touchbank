package com.touchbank.adapters;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.touchbank.R;
import com.touchbank.models.Transaction;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Yurii Andrusiak[yuriy.andrusyak@gmail.com] on 28.06.2015.
 */
public class TransactionRecyclerAdapter extends RecyclerView.Adapter {

    private static final int TYPE_TRANSACTION = 101;
    private static final int TYPE_DATE = 102;
    int[] colorSet;

    ArrayList<Object> items;
    ArrayList<Integer> bgColors = new ArrayList<>();

    public TransactionRecyclerAdapter(ArrayList<Object> transactions) {
        items = transactions;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //init a color set
        if(colorSet==null) {
            generateColors(parent.getResources());
        }

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder vh =null;
        switch (viewType){
            case TYPE_DATE:
                View v = inflater.inflate(R.layout.fragment_transaction_list_date, parent, false);
                vh = new DateViewHolder(v);
                break;
            case TYPE_TRANSACTION:
                v = inflater.inflate(R.layout.fragment_transaction_list_item, parent, false);
                vh = new TransactionViewHolder(v);
                break;
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if(viewHolder instanceof  DateViewHolder) {
            DateViewHolder holder = (DateViewHolder) viewHolder;
            String date = (String) items.get(position);
            holder.dateView.setText(date);

            // set background color
            holder.itemView.setBackgroundColor(colorSet[bgColors.get(position)]);

        } else if(viewHolder instanceof  TransactionViewHolder) {

            TransactionViewHolder holder = (TransactionViewHolder) viewHolder;
            Transaction t = (Transaction) items.get(position);

            holder.time.setText(t.getFormattedTime());
            holder.title.setText(t.getTitle());

            // Split ampunt value into the integral and fractional parts
            String amountStr =String.format("%.2f", t.getAmount());
            String[] amount = amountStr.split("[,.]");
            holder.amount.setText(amount[0]);
            holder.amountCoins.setText(amount[1]);

            // set background color
            int color = bgColors.get(position);
            int anotherColor = color==0?1:0;
            holder.itemView.setBackgroundColor(colorSet[color]);
            holder.delimiter.setBackgroundColor(colorSet[anotherColor]);

            if (t.getType() != Transaction.Category.CYAN) {
//            holder.icon.setD.setBackgroundResource(R.color.material_deep_teal_500);
            }
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(items.get(position) instanceof Transaction){
            return TYPE_TRANSACTION;
        } else if(items.get(position) instanceof String){
            return TYPE_DATE;
        } else {
            throw new RuntimeException("Invalid class in "+this.getClass().getSimpleName());
        }
    }

    private void generateColors(Resources resources) {
        colorSet = new int[2];
        colorSet[0] = resources.getColor(R.color.grey_bg);
        colorSet[1] = resources.getColor(R.color.white);

        int type=1;
        for(Object item: items){
            if(item instanceof String) {
                //if new date is detected then change the color
                type= type==0 ? 1 : 0;
            }
            bgColors.add(type);
        }
    }

    public static class DateViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.dateView)
        TextView dateView;

        public DateViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public static class TransactionViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.icon)
        ImageView icon;
        @Bind(R.id.time)
        TextView time;
        @Bind(R.id.title)
        TextView title;
        @Bind(R.id.amount)
        TextView amount;
        @Bind(R.id.amountCoins)
        TextView amountCoins;
        @Bind(R.id.delimiter)
        View delimiter;

        public TransactionViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}